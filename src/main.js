const axios = require('axios');

async function main () {
    try {
        const res = await axios.get('https://jsonplaceholder.typicode.com/users');
        console.log(res.data);
    } catch (e) {
        console.log(e);
    }
}

main();
